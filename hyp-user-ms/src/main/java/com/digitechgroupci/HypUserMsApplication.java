package com.digitechgroupci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HypUserMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(HypUserMsApplication.class, args);
    }

}
